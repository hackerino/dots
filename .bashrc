#!/bin/bash

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

set -o vi

complete -cf sudo
complete -cf man
complete -c wv

export BROWSER=firefox
export MOZ_ENABLE_WAYLAND=1
export CARGO_HOME="$HOME/.cargo"
export RUSTC_WRAPPER="sccache"
export LOFI_FILE=$HOME/.lofi_urls
export EDITOR='nvim'
export QT_QPA_PLATFORMTHEME=gtk2
export ZETTEL_PATH="$HOME/Documenti/Zettel"

PS1='[\u@\h \W]\$ '

# nnn
export NNN_BMS='m:/media/;s:~/Calderone/sito'
export NNN_PLUG='d:dragdrop;o:fzopen;p:preview-tui;r:pdfread;u:gitplugs;w:wall'
export NNN_FIFO=/tmp/nnn.fifo

n() {
  export NNN_TMPFILE=${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd

  nnn -d "$@"

  if [ -f $NNN_TMPFILE ]; then
    . $NNN_TMPFILE
    rm -f $NNN_TMPFILE >/dev/null
  fi
}

wv() {
  if [ $# -eq 1 ]; then
    nvim $(which "$1")
  fi
}

yd() {
  (($# > 2 || $# == 0)) && echo "Usage: yd [max_resolution] url" && return

  res=720
  (($# == 2)) && res=$1 && shift

  yt-dlp --embed-metadata -f "bestvideo[height<=$res]+bestaudio/best[height<=$res]" "$1"
}

source_if_present() {
  [ -f "$1" ] && . "$1"
}

lfcd() {
  # `command` is needed in case `lfcd` is aliased to `lf`
  cd "$(command lf -print-last-dir "$@")" || exit
}

command_not_found="/usr/share/doc/pkgfile/command-not-found.bash"

source_if_present "$HOME/.local/bin/git-prompt.sh"
source_if_present "$HOME/.bash_aliases"
source_if_present "$HOME/.config/exercism/exercism_completion.bash"
source_if_present "$command_not_found"
source_if_present "/usr/share/fzf/key-bindings.bash"
source_if_present "/usr/share/fzf/completion.bash"
source_if_present "$HOME/.cargo/env"
source_if_present "$HOME/.fzf.bash"

eval "$(zoxide init bash)"
eval "$(starship init bash)"

# Non standard config
source_if_present $HOME/.custom

export PATH="$CUSTOM_PATH:$PATH"
export PATH="$(composer config -g home)/vendor/bin:$PATH"
export PATH="$PATH:/home/simone/.local/bin"

export CLASSPATH=".:/usr/share/java/antlr-complete.jar:$CLASSPATH"
