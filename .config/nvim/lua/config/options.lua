vim.cmd.colorscheme("catppuccin-mocha")

vim.opt.shiftwidth = 2
vim.opt.number = true
vim.opt.relativenumber = true
