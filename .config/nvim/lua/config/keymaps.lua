local builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>ta', builtin.find_files, { desc = 'Telescope find files' })
vim.keymap.set('n', '<leader>to', builtin.live_grep, { desc = 'Telescope live grep' })
vim.keymap.set('n', '<leader>te', builtin.buffers, { desc = 'Telescope buffers' })
vim.keymap.set('n', '<leader>tu', builtin.help_tags, { desc = 'Telescope help tags' })

local oil = require('oil')
vim.keymap.set("n", "<leader>e", oil.open)
