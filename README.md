# dots
## Description
Just the dotfiles I like, mainly built around **sway** (but I also like **GNOME**). 

## Contributing
If you have any suggestion to improve them, let me know. I would certainly love
to hear that these dotfiles are being useful to others.

## Authors
Simone Dotto (me)

## License
Public domain

